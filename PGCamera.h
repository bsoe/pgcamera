//==============================================================================
/*

*/
//==============================================================================

//------------------------------------------------------------------------------
#include <flycapture/FlyCapture2.h>

#include <string>

//------------------------------------------------------------------------------
//using namespace std;
//using namespace cv;
//------------------------------------------------------------------------------

class PGCamera {

public:

    PGCamera();

    ~PGCamera();

    /** initialize the camera */
    void connect(const unsigned int camera_index = 0);

    /** start capturing images */
    void startCapture();

    /** stop capturing images */
    void stopCapture();

    /** shutdown */
    void disconnect();

    /** get initialized camera info */
    void printCameraInfo();

    /** get initialized cameras ID */
    unsigned int getID();

    /** set the video frame rate. set after initialization. */
    void setFrameRate(const unsigned int frame_rate);

    /** capture a single image and save to file.
     *  will automatically be named if empty
     *  the currently supported pixel formats are: RAW8, MONO8, or RGB8*/
    void captureImage(FlyCapture2::PixelFormat format, std::string file_name = "");

    //=================
    // static functions
    //=================
    static unsigned int getNumCameras();

    static void printBuildInfo();

private:

    static int CheckError( FlyCapture2::Error error );

    //FlyCapture2::Camera camera;
    FlyCapture2::Camera camera;
    FlyCapture2::CameraInfo camera_info;

    FlyCapture2::Image rawImage;
    FlyCapture2::Image convertedImage;

    int image_number = 0;

    bool connected = false;
    bool capturing = false;


};



