//==============================================================================
/*

*/
//==============================================================================

#include "PGCamera.h"
#include "stdafx.h"

//------------------------------------------------------------------------------

#include <iostream>
#include <stdexcept>

PGCamera::PGCamera()
{
    // Since this application saves images in the current folder
    // we must ensure that we have permission to write to this folder.
    // If we do not have permission, fail right away.
    FILE* tempFile = fopen("test.txt", "w+");
    if (tempFile == NULL)
    {
        throw std::runtime_error("Failed to create file in current folder.  Please check permissions.\n");
    }
    fclose(tempFile);
    remove("test.txt");
}

PGCamera::~PGCamera()
{
    FlyCapture2::Error error;
    if (capturing){
        error = camera.StopCapture();
        CheckError(error);
    }
    if (connected){
        error = camera.Disconnect();
        CheckError(error);
    }
}

void PGCamera::connect(const unsigned int camera_index)
{
    FlyCapture2::Error error;

    FlyCapture2::BusManager busMgr;
    FlyCapture2::PGRGuid guid;
    error = busMgr.GetCameraFromIndex(camera_index, &guid);
    CheckError(error);

    error = camera.Connect(&guid);
    CheckError(error);

    error = camera.GetCameraInfo(&camera_info);
    CheckError(error);

    connected = true;
}

void PGCamera::disconnect()
{
    FlyCapture2::Error error;
    error = camera.Disconnect();
    CheckError(error);
    connected = false;
}

void PGCamera::startCapture()
{
    FlyCapture2::Error error;
    error = camera.StartCapture();
    CheckError(error);
    capturing = true;
}

void PGCamera::stopCapture()
{
    FlyCapture2::Error error;
    error = camera.StopCapture();
    CheckError(error);
    capturing = false;
}

void PGCamera::printCameraInfo()
{
    printf(
        "\n*** CAMERA INFORMATION ***\n"
        "Serial number - %u\n"
        "Camera model - %s\n"
        "Camera vendor - %s\n"
        "Sensor - %s\n"
        "Resolution - %s\n"
        "Firmware version - %s\n"
        "Firmware build time - %s\n\n",
        camera_info.serialNumber,
        camera_info.modelName,
        camera_info.vendorName,
        camera_info.sensorInfo,
        camera_info.sensorResolution,
        camera_info.firmwareVersion,
        camera_info.firmwareBuildTime );
}

unsigned int PGCamera::getID()
{
    return camera_info.serialNumber;
}

void PGCamera::setFrameRate(const unsigned int frame_rate)
{
    FlyCapture2::Error error;

    FlyCapture2::Property property;
    property.type = FlyCapture2::FRAME_RATE;
    error = camera.GetProperty( &property );
    CheckError(error);

    property.onOff = true;
    property.autoManualMode = false;
    property.absControl = true;
    property.absValue = frame_rate;
    error = camera.SetProperty(&property);
    CheckError(error);

    std::cout << "set frame rate" << std::endl;
}

/*
void PGCamera::setImage(const unsigned int width, const unsigned int height,
                        const unsigned int offset_w, const unsigned int offset_h)
{
    FlyCapture2::Error error;

    FlyCapture2::Format7ImageSettings settings;
    error = camera.GetFormat7Configuration(&settings, ?, ?);
    CheckError(error);

    settings.width = width;
    settings.height = height;
    settings.offsetX = offset_w;
    settings.offsetY = offset_h;
    settings.pixelFormat = FlyCapture2::PIXEL_FORMAT_RAW8;

    error = camera.SetFormat7Configuration((&settings);
    CheckError(error);
}


void PGCamera::setMode(FlyCapture2::Mode mode)
{
    FlyCapture2::Error error;

    FlyCapture2::Format7ImageSettings settings;
    error = camera.GetFormat7Configuration(&settings, ?, ?);
    CheckError(error);

    settings.mode = mode;

    error = camera.SetFormat7Configuration((&settings);
    CheckError(error);
}
*/


void PGCamera::captureImage(FlyCapture2::PixelFormat format, std::string file_name)
{
    FlyCapture2::Error error;

    // Retrieve an image
    error = camera.RetrieveBuffer( &rawImage );
    CheckError(error);

    printf( "Grabbed image %d\n", image_number );

    if (file_name.empty()){
        // Create a unique filename
        char generated_name[512] = {0};
        sprintf( generated_name, "image-%u-%d.pgm", camera_info.serialNumber, image_number );
        file_name = std::string(generated_name);
    }

    switch (format)
    {
        case FlyCapture2::PIXEL_FORMAT_RAW8:
        {
            error = rawImage.Save( file_name.c_str() );
            CheckError(error);
            break;
        }
        case FlyCapture2::PIXEL_FORMAT_MONO8:
        {
            error = rawImage.Convert( FlyCapture2::PIXEL_FORMAT_MONO8, &convertedImage );
            CheckError(error);

            error = convertedImage.Save( file_name.c_str() );
            CheckError(error);
            break;
        }
        case FlyCapture2::PIXEL_FORMAT_RGB8:
        {
            error = rawImage.Convert( FlyCapture2::PIXEL_FORMAT_RGB8, &convertedImage );
            CheckError(error);

            error = convertedImage.Save( file_name.c_str() );
            CheckError(error);
            break;
        }
        default :
            std::cout << "Unsupported pixel format. Choose RAW8, MONO8, or RGB8" << std::endl;
    }
    /*

    // Convert the raw image
    error = rawImage.Convert( FlyCapture2::PIXEL_FORMAT_RGB8, &convertedImage );
    //CheckError(error);

    if (file_name.empty())
    {
        // Create a unique filename
        char generated_name[512];
        sprintf( generated_name, "image-%u-%d.pgm", camera_info.serialNumber, image_number );

        // Save the image. If a file format is not passed in, then the file
        // extension is parsed to attempt to determine the file format.
        error = rawImage.Save( generated_name );
        //error = convertedImage.Save( generated_name );
        CheckError(error);
    }
    else {
        error = rawImage.Save( file_name.c_str() );
        //error = convertedImage.Save( generated_name );
        CheckError(error);
    }
    */

    ++image_number;
}




//==============================================
// STATIC FUNCTIONS
//==============================================

unsigned int PGCamera::getNumCameras()
{
    FlyCapture2::Error error;
    FlyCapture2::BusManager busMgr;
    unsigned int numCameras = 0;
    error = busMgr.GetNumOfCameras(&numCameras);
    CheckError(error);

    return numCameras;
}

void PGCamera::printBuildInfo()
{
    FlyCapture2::FC2Version fc2Version;
    FlyCapture2::Utilities::GetLibraryVersion( &fc2Version );
    char version[128];
    sprintf(
        version,
        "FlyCapture2 library version: %d.%d.%d.%d\n",
        fc2Version.major, fc2Version.minor, fc2Version.type, fc2Version.build );

    printf( version );

    char timeStamp[512];
    sprintf( timeStamp, "Application build date: %s %s\n\n", __DATE__, __TIME__ );

    printf( timeStamp );
}

int PGCamera::CheckError( FlyCapture2::Error error )
{
    if (error != FlyCapture2::PGRERROR_OK)
    {
        error.PrintErrorTrace();
        return -1;
    }
}
