#include "PGCamera.h"

#include <iostream>

int main(int, char**)
{
    PGCamera::printBuildInfo();

    std::cout << "number of cameras detected: "
              << PGCamera::getNumCameras() << std::endl;

    PGCamera camera;
    camera.connect(0);
    camera.printCameraInfo();
    camera.startCapture();

    for (int i=0;i<10;++i){
        camera.captureImage(FlyCapture2::PIXEL_FORMAT_RAW8);
    }

    camera.stopCapture();
    camera.disconnect();

    return 0;
}
