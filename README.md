Wrapper for Point Grey Camera to save images
============================================

Authors : Brian Soe  
Contact : bsoe at stanford.edu

# Dependencies

- flycapture (point greys)

# About

Uses Opencv to stream from the point grey blackfly.  

The code was developed for [Point Grey Blackfly GigE Camera].  
See [Blackfly Setup.md] for instructions on setting a persistent IP.


# Installation

1. Install [Flycapture for Ubuntu 12.04] or [Flycapture for Ubuntu 14.04].
2. Set the [USB settings] or Ethernet settings. 
3. Check that the camera works in flycap.
4. Build and run this program .

### 3. Build

```sh
mkdir build
cd build
cmake ..
make
```


[USB settings]:http://www.ptgrey.com/KB/10685
[Flycapture for Ubuntu 12.04]:https://www.dropbox.com/s/v11ggo6qexqvtc3/flycapture2-2.6.3.4-amd64-pkg.tgz?dl=0
[Flycapture for Ubuntu 14.04]:https://www.ptgrey.com/flycapture-sdk